/**
  * @desc locale.js contains all the strings of the application. This file can
  * also be used to implement i18n.
*/

export default{
  categoryPage: {
    heading: 'Women\'s tops',
  },
  error: {
    serverError: 'Error occured while fetching response',
  },
};
